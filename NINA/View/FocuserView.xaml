﻿<!--
    Copyright © 2016 - 2019 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.FocuserView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Locale"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">
    <GroupBox>
        <GroupBox.Header>
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="2*" />
                    <ColumnDefinition Width="3*" />
                </Grid.ColumnDefinitions>
                <TextBlock
                    VerticalAlignment="Center"
                    FontSize="20"
                    Text="{ns:Loc LblFocuser}" />
                <Grid Grid.Column="1" Margin="5">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="40" />
                        <ColumnDefinition />
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="Auto" />
                    </Grid.ColumnDefinitions>
                    <ninactrl:LoadingControl
                        Height="30"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center"
                        LoadingImageBrush="{StaticResource PrimaryBrush}"
                        Visibility="{Binding ChooseFocuserCommand.Execution.IsNotCompleted, Converter={StaticResource BooleanToVisibilityCollapsedConverter}, FallbackValue=Collapsed}" />
                    <ComboBox
                        Grid.Column="1"
                        MinHeight="40"
                        DisplayMemberPath="Name"
                        IsEnabled="{Binding FocuserInfo.Connected, Converter={StaticResource InverseBooleanConverter}}"
                        ItemsSource="{Binding FocuserChooserVM.Devices}"
                        SelectedItem="{Binding FocuserChooserVM.SelectedDevice}"
                        SelectedValuePath="Name" />
                    <Button
                        Grid.Column="2"
                        Width="40"
                        Height="40"
                        Margin="1,0,0,0"
                        Command="{Binding FocuserChooserVM.SetupDialogCommand}"
                        IsEnabled="{Binding FocuserChooserVM.SelectedDevice.HasSetupDialog}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="False">
                                <TextBlock Text="{ns:Loc LblSettings}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Grid>
                            <Path
                                Margin="5"
                                Data="{StaticResource SettingsSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Grid>
                    </Button>
                    <Button
                        Grid.Column="3"
                        Width="40"
                        Height="40"
                        Margin="1,0,0,0"
                        Command="{Binding RefreshFocuserListCommand}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="False">
                                <TextBlock Text="{ns:Loc LblRescanDevices}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Grid>
                            <Path
                                Margin="5"
                                Data="{StaticResource LoopSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Grid>
                    </Button>
                    <Grid
                        Grid.Column="4"
                        Width="40"
                        Height="40"
                        Margin="1,0,0,0"
                        HorizontalAlignment="Center"
                        VerticalAlignment="Center">
                        <ninactrl:CancellableButton
                            ButtonForegroundBrush="{StaticResource ButtonForegroundDisabledBrush}"
                            ButtonImage="{StaticResource PowerSVG}"
                            CancelButtonImage="{StaticResource CancelSVG}"
                            CancelCommand="{Binding CancelChooseFocuserCommand}"
                            Command="{Binding ChooseFocuserCommand}"
                            IsEnabled="{Binding FocuserInfo.Connected, Converter={StaticResource InverseBooleanConverter}}"
                            ToolTip="{ns:Loc LblConnect}"
                            Visibility="{Binding FocuserInfo.Connected, Converter={StaticResource InverseBoolToVisibilityConverter}}" />
                        <Grid Visibility="{Binding Focuser, Converter={StaticResource NullToVisibilityConverter}}">
                            <Button
                                Command="{Binding DisconnectCommand}"
                                IsEnabled="{Binding FocuserInfo.Connected}"
                                Visibility="{Binding FocuserInfo.Connected, Converter={StaticResource VisibilityConverter}}">
                                <Button.ToolTip>
                                    <ToolTip ToolTipService.ShowOnDisabled="False">
                                        <TextBlock Text="{ns:Loc LblDisconnect}" />
                                    </ToolTip>
                                </Button.ToolTip>
                                <Grid>
                                    <Path
                                        Margin="5"
                                        Data="{StaticResource PowerSVG}"
                                        Fill="{StaticResource ButtonForegroundBrush}"
                                        Stretch="Uniform" />
                                </Grid>
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </GroupBox.Header>

        <Grid>
            <StackPanel>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblName}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding FocuserInfo.Name}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblDescription}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding Focuser.Description}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblDriverInfo}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding Focuser.DriverInfo}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblDriverVersion}" />
                            <TextBlock Margin="5,0,0,0" Text="{Binding Focuser.DriverVersion}" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblIsMoving}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding FocuserInfo.IsMoving}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblMaxIncrement}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding Focuser.MaxIncrement}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblMaxStep}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding Focuser.MaxStep}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblPosition}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding FocuserInfo.Position}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border
                    BorderBrush="{StaticResource BorderBrush}"
                    BorderThickness="0"
                    IsEnabled="{Binding Focuser.TempCompAvailable}">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblTemperatureCompensation}" />
                            <CheckBox
                                Width="75"
                                Height="25"
                                Margin="5,0,0,0"
                                HorizontalAlignment="Left"
                                Command="{Binding ToggleTempCompCommand}"
                                CommandParameter="{Binding Path=IsChecked, RelativeSource={RelativeSource Self}}"
                                IsChecked="{Binding FocuserInfo.TempComp}" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>
                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock Text="{ns:Loc LblTemperature}" />
                            <TextBlock
                                Margin="5,0,0,0"
                                Text="{Binding FocuserInfo.Temperature, StringFormat=\{0:0.00\}}"
                                TextWrapping="WrapWithOverflow" />
                        </UniformGrid>
                    </UniformGrid>
                </Border>

                <Border BorderBrush="{StaticResource BorderBrush}" BorderThickness="0">
                    <UniformGrid Columns="2">
                        <UniformGrid
                            Margin="0,6,0,6"
                            VerticalAlignment="Center"
                            Columns="2">
                            <TextBlock VerticalAlignment="Center" Text="{ns:Loc LblTargetPosition}" />
                            <StackPanel Orientation="Horizontal">
                                <TextBox
                                    Height="25"
                                    MinWidth="90"
                                    Margin="5,0,0,0"
                                    Text="{Binding TargetPosition}" />
                                <Grid IsEnabled="{Binding Focuser, Converter={StaticResource InverseNullToBooleanConverter}}">
                                    <ninactrl:CancellableButton
                                        Width="50"
                                        Height="25"
                                        Margin="5"
                                        HorizontalAlignment="Left"
                                        ButtonText="{ns:Loc LblMove}"
                                        CancelButtonImage="{StaticResource CancelSVG}"
                                        CancelCommand="{Binding HaltFocuserCommand}"
                                        Command="{Binding MoveFocuserCommand}"
                                        ToolTip="" />
                                </Grid>
                            </StackPanel>
                        </UniformGrid>
                    </UniformGrid>
                </Border>
            </StackPanel>
        </Grid>
    </GroupBox>
</UserControl>