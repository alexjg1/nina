﻿<!--
    Copyright © 2016 - 2019 Stefan Berg <isbeorn86+NINA@googlemail.com>

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    N.I.N.A. is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    N.I.N.A. is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with N.I.N.A..  If not, see http://www.gnu.org/licenses/.-->
<UserControl
    x:Class="NINA.View.AnchorableImageStatisticsView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:local="clr-namespace:NINA.View"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:ns="clr-namespace:NINA.Locale"
    d:DesignHeight="300"
    d:DesignWidth="300"
    mc:Ignorable="d">
    <ScrollViewer VerticalScrollBarVisibility="Auto">
        <Grid>
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition />
            </Grid.RowDefinitions>

            <StackPanel>
                <StackPanel
                    Margin="5"
                    VerticalAlignment="Top"
                    DataContext="{Binding Statistics}">
                    <UniformGrid Columns="2">
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblWidth}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding Width, StringFormat=\{0:#0\}}" />
                        </StackPanel>
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblHeight}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding Height, StringFormat=\{0:#\}}" />
                        </StackPanel>
                    </UniformGrid>
                    <UniformGrid Columns="2">
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblMean}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding Mean, StringFormat=\{0:0.00\}}" />
                        </StackPanel>
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblStDev}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding StDev, StringFormat=\{0:0.00\}}" />
                        </StackPanel>
                    </UniformGrid>

                    <UniformGrid Columns="2">
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblMedian}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding Median, StringFormat=\{0:0.00\}}" />
                        </StackPanel>
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblMAD}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding MedianAbsoluteDeviation, StringFormat=\{0:0.00\}}" />
                        </StackPanel>
                    </UniformGrid>
                    <UniformGrid Columns="2">
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblMin}" />
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding Min}" />
                                <TextBlock Text=" (" />
                                <TextBlock
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding MinOccurrences}" />
                                <TextBlock Text="x)" />
                            </StackPanel>
                        </StackPanel>
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblMax}" />
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding Max}" />
                                <TextBlock Text=" (" />
                                <TextBlock
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding MaxOccurrences}" />
                                <TextBlock Text="x)" />
                            </StackPanel>
                        </StackPanel>
                    </UniformGrid>
                    <UniformGrid Columns="2">
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblNrStars}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding DetectedStars, StringFormat=\{0:#\}}" />
                        </StackPanel>
                        <StackPanel Orientation="Horizontal">
                            <TextBlock
                                Width="55"
                                VerticalAlignment="Center"
                                Text="{ns:Loc LblHFR}" />
                            <TextBlock
                                Width="70"
                                HorizontalAlignment="Center"
                                VerticalAlignment="Center"
                                Text="{Binding HFR, StringFormat=\{0:0.00\}}" />
                        </StackPanel>
                    </UniformGrid>
                    <StackPanel Orientation="Horizontal">
                        <TextBlock
                            Width="55"
                            VerticalAlignment="Center"
                            Text="{ns:Loc LblBitDepth}" />
                        <TextBlock
                            Width="70"
                            HorizontalAlignment="Center"
                            VerticalAlignment="Center"
                            Text="{Binding BitDepth}" />
                    </StackPanel>
                </StackPanel>
                <Expander
                    BorderBrush="{StaticResource BorderBrush}"
                    BorderThickness="0"
                    Header="{ns:Loc LblExposureCalculator}">
                    <Border>
                        <StackPanel Margin="5" VerticalAlignment="Top">
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    Width="170"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblMinOptimalADU}" />
                                <TextBlock
                                    Width="40"
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding MinExposureADU, StringFormat=\{0:#0\}, Mode=OneWay}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    Width="170"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblMinimumExposureTime}" />
                                <TextBlock
                                    Width="40"
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding MinimumRecommendedExposureTime, StringFormat=\{0:#0.00\}, Mode=OneWay}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    Width="170"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblMaxOptimalADU}" />
                                <TextBlock
                                    Width="40"
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding MaxExposureADU, StringFormat=\{0:#0\}, Mode=OneWay}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    Width="170"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblMaximumExposureTime}" />
                                <TextBlock
                                    Width="40"
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding MaximumRecommendedExposureTime, StringFormat=\{0:#0.00\}, Mode=OneWay}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    Width="170"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblOptimalExposureTime}" />
                                <TextBlock
                                    Width="40"
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding OptimizedExposureTime, StringFormat=\{0:#0.00\}, Mode=OneWay}" />
                            </StackPanel>
                            <StackPanel Orientation="Horizontal">
                                <TextBlock
                                    Width="170"
                                    VerticalAlignment="Center"
                                    Text="{ns:Loc LblCurrentDownloadToDataRatio}" />
                                <TextBlock
                                    Width="40"
                                    HorizontalAlignment="Center"
                                    VerticalAlignment="Center"
                                    Text="{Binding CurrentDownloadToDataRatio, Mode=OneWay}" />
                            </StackPanel>
                        </StackPanel>
                    </Border>
                </Expander>
            </StackPanel>

            <local:HistogramView
                Grid.Row="1"
                MinHeight="80"
                DataContext="{Binding Statistics}" />
        </Grid>
    </ScrollViewer>
</UserControl>